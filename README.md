# Introduction
Course: Angular - The Complete Guide (2021 Edition)

- **ng4-complete-guide: Course Project**
- my-first-app: First Angular App with simple examples on ngFor, ngIf
- cmp-databinding-start: Sample project on data binding and passing data between components
- services-start: Sample project on using Services
- routing-start: Sample project on using Routes
- forms-td-start: Sample project on Template Driven Form approach
- forms-reactive-start: Sample project on Reactive Driven Form approach
    

# References
- https://github.com/angular/angular-cli
- https://angular.io/api/core/Renderer2
- https://academind.com/learn/javascript/understanding-rxjs/ (Observables)
- https://angular.io/api/forms/Validators (Forms)
- https://angular.io/api?type=directive (Forms)

# Useful commands

```shell
ng serve --host 0.0.0.0
npm install
```

# Project
## Requirements
* Shopping List Feature
    * Shopping list
    * Shopping List Edit
* Recipe Book Feature
    * Recipe List
    * Recipe Item
    * Recipe Detail
* Models
    * Ingredient
    * Recipe

## Notes
* `ng generate c COMPONENT --skipTests true` : skips spec file
* `ng g c recipes/recipe-list --skipTests true` : create the component in a nested folder 
* see `header` for examples on navigation bar

# Notes

## Installing
  ```shell
  $ npm install -g @angular/cli@latest
  $ ng new my-frst-app --no-strict
  $ ng serve
  ```
  
## Versioning
  * AngularJS = Angular 1
  * Angular  = Angular 2+; Released every 6 months

## General
  * **package.json**: dependencies
  * `<app-root></app-root>`: creates a special HTML tag that loads all of the Angular code.
  In `index.html`, the app-root tag is used which loads the Angular app. 
  * To add modules, add it to app.module.ts as import and @NgModule imports block.
  * Adding Bootstrap CSS:
      * `npm install --save bootstrap@3`
      *  Update `angular.json` style section with the new path of the css file
  * `main.ts` is the main entry point from `index.html`
    * Then, it goes to `app.module.ts`
    * From `bootstrap`: [COMPONENTS], it loads the rest of the components
  * You want to use the app component as the starting point for other components.
    * Create a directory under `app` for each component
  * Debugging:
    * (Dev mode only): To apply Breakpoint to the TS files, open Dev Tools > Sources > webpack://

## Component
  * Just a typescript class
  * To add it to AppComponent, update `app.module.ts`
  * Add to `@NgModule.declarations` and import {}
  * To use it in AppComponent.html, use the `selector` definition
  * Minimally, you want a `html` and `ts` class
  * The automatic way to generate it is `$ ng generate component <NAME>`
  * Instead of using `templateUrl`, you can include the entire HTML content by updating it to `template: '<htmlTags></htmlTags>'`. 
This is inline template approach.
    * You can create a `selector` as an attribute instead of an element by enclosing it in `[]`. E.g., `selector: [app-servers]`
    then use it by `<div app-servers></div>`. Not common use case.
    * You can create a `selector` as a class instead of an element by enclosing prefixing it with a `.`. E.g., `selector: .app-servers`
    then use it by `<div class="app-servers"></div>`. Not common use case.
    * You can use backticks to create multiline Javascript contents. This would be useful for inline declarations like `template` 
    and `style` (instead of `styleUrls`).
    * By default, the properties are only visible for the component unless you explicitly expose them with `@Input()`

## Lifecycle
* Hooks:
  * (1) ngOnChanges: called after a bound input property changes. It's called at the start and then multiple times. `@Input`
    * Useful if you want to see/act upon the previous state's information.
  * (2) ngOnInit: called once the component is initialized. Has not added to the DOM yet. Runs after the constructor.
  * (3) ngDoCheck: called during every change detection run. Runs a lot whenever change detection runs. Change detection
    is the system by which Angular determines whether something changed on the template of the component or an event occurred.
  * (4) ngAfterContentInit: called after content (ng-content) has been projected into view.
  * (5) ngAfterContentChecked: called every time the projected content has been checked.
  * (6) ngAfterViewInit: called after the component's view (and child views) has been initialized.
  * (7) ngAfterViewChecked: called every time the view (and child views) have been checked.
  * (8) ngOnDestroy: called once the component is about to be destroyed.

* Services & Dependency Injection
  * You should not create the Service manually. Put it in the constructor and add it in the providers Component block.
  * Hierarchical Injector
    * AppModule -> If the service is injected into the main module, then the same Instance of the Service is available **Application-wide**.
    * AppComponent -> If the service is injected into a component that's on a lower level than the AppModule, then the Same Instance of the Service is available for all Components but not for other Services
    * Any other Component -> If the service is injected for the Component, then the Service is availiable for the component
      and all of its child components.
    * Key: The same instance of the Service propagates down but not up.
      * If you want to have the same instance of the Service in multiple components, then you'll want to remove the Service in the providers block but still leave it in the constructor.
  * `@Injectable`: Use this to add other Services. It's like `@Autowired`.

## Databinding
  * Communication between Typescript(Business Logic) and Template(HTML)
  * TS ---> HTML:
      * String interpolation `{{ data }}`
      * Property binding `[property] = "data"`
  * HTML <--- TS, react to user events: 
      * Event Binding `(event)="expression"`
  * HTML <--> TS
      * `[(ngModel)]="data"`
  * In the HTML, `{{ }}` is String interpolation of Typescript code. It MUST return a String as the final product. Use
    if you want to output a text in the template
  * In the HTML `[]` means to bind properties to a html attribute. Use if you want to change property, attribute, etc.
  * In the HTML `()` lets you bind a html event e.g., `onClick` as `click=(placeholder)` that will execute your component's
  function.
  * `$event` is a reserved word for the HTML event that can be used to extract more details on the event.
  ```html
  <input
    type="text"
    class="form-control"
    (input)="onUpdateServerName($event)">

  onUpdateServerName(event: any) {
      console.log(event)
      this.serverName = (<HTMLInputElement>event.target).value;
  }
  ```
  * In the HTML `[()]` lets you perform two-way binding

    
## Directives
  * Instructions in the DOM!
  * `*ngIf`: used in the attribute. * means it's a structural directive because it changes the DOM.
    * Behind the scenes, Angular converts the star into ng-template. E.g.,
    ```angular2html
    <ng-template [ngIf]="conditional">
      <div>Contents here...</div>
    </ng-template>
    ```
  * `[ngSwitch]`:
    ```angular2html
    <div [ngSwitch]="value">
      <p *ngSwitchCase="5">Value is 5</p>
      <p *ngSwitchDefault>Value is default</p>
    </div>
    ```
  * Attribute directives don't add or remove elements. They only change the element they were placed on.
  * Example for loop:
  ```javascript
    <li *ngFor="let entry of clickedTimestamp; let i = index"
        [ngStyle]="{backgroundColor: i >= 5 ? 'blue' : 'transparent'}"
        [ngClass]="{'white-text': i >= 5}" >
      {{ entry }}
    </li>
  ```
  * Custom Directive
    * TS:
    ```typescript
    import {Directive, ElementRef, HostBinding, HostListener, OnInit, Renderer2} from "@angular/core";
    
    @Directive({
    selector: '[appBetterHighlight]' // [] let you use the directive without the square brackets to an element
    })
    export class BetterHighlightDirective implements OnInit {
    // access the corresponding property for the element
    @HostBinding('style.backgroundColor') backgroundColor: string = 'transparent'; // Sets a default if it's null in the beginning
    
    constructor(private elRef: ElementRef, private renderer: Renderer2) {
    
    }
    ngOnInit(): void {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
    }
    
    @HostListener('mouseenter') mouseover(eventData: Event) {
    // One way to do this is through renderer
    // this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue')
    // Another way is to do it through
    this.backgroundColor = 'blue';
    }
    
    @HostListener('mouseleave') mouseleave(eventData: Event) {
    // this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent')
    this.backgroundColor = 'transparent';
    }
    }
    ```

    * HTML:
    ```angular2html
    <p appBetterHighlight [defaultColor]="'cyan'" [highlightColor]="'orange'">Style me</p>
    ```
  * Custom Structural Directive
    ```typescript
      @Directive({
      selector: '[appUnless]'
      })
      export class UnlessDirective {
      // Creates a custom function for this input
      // The method name HAS to be the same as the selector name
      @Input() set appUnless(condition: boolean) {
      if(!condition) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
      } else {
      this.viewContainerRef.clear();
      }
      }
    
      // templateRef - what
      // viewContainerRef- where
      constructor(private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {
    
      }
      }
    ```
    ```angular2html
    <div *appUnless="onlyOdd">
    ```


## Custom Events
  * Use EventEmitters to emit a change event that is listened to by the parent component.
  
  Example parent HTML:
  ```angular2html
    <!-- In parent component    -->
    <app-child
        (serverCreated)="onServerAdded($event)"
        (blueprintCreated)="onBlueprintAdded($event)">
     </app-child>
  ```
  
  Example parent typescript:
  ```typescript
    onServerAdded(serverData: { serverName: string, serverContent: string }) {
      this.serverElements.push({
        type: 'server',
        name: serverData.serverName,
        content: serverData.serverContent
      });
    }
  ```
  
  Example app-child component:
  ```typescript
   @Output() serverCreated = new EventEmitter<{serverName: string, serverContent: string }>();
    onAddServer() {
      this.serverCreated.emit({
        serverName: this.newServerName,
        serverContent: this.newServerContent})
    }
  ```
  
* Local References in Templates
  * Useful if you want to reuse the same reference in the template. The reference is for the entire HTMl element and 
  can only be used in the same template.
  ```angular2html
    <input ... #serverNameInput>
    <!-- elsewhere in the code... -->
    <button (click)="onAddServer(serverNameInput)"></button>    
  ```
  
  * Another way is to use `@ViewChild()`, where you can put a reference in the template and then use that reference
  identifier in the typescript code. It is bad practice to set a value for the reference. You can also use it to grab the
  component as well.
  ```typescript
    @ViewChild('serverNameInput') serverContentInput: ElementRef
     // elsewhere in the code
    const whatever = this.serverNameInput.nativeElement.value
  ```
  
* `ng-content`
  * Allows you to put a placeholder in the child component where the contents of the child component is rendered.
    app-child-component:
      ```angular2html
      <ng-content> </ng-content>
      ```
    app-parent-component
    ```angular2html
    <app-child-component>
      This is the content that I want to render within the ng-content tag
    </app-child-component>
    ```
    
* Input()
  * Parent Component:
  ```angular2html
      <app-recipe-item *ngFor="let recipe of recipes"
                        [recipe] = "recipe">
      </app-recipe-item>
  ```
  * Child Component:
  ```typescript
  export class RecipeItemComponent implements OnInit {
    @Input() recipe: Recipe
  }
  ```

## Routing
  * Where should we add routes?
    * app.module.ts!
      ```typescript
        import {Routes} from "@angular/router";
        
        const appRoutes: Routes = [
        { path: '', component: HomeComponent },
        { path: 'users', component: UsersComponent },
        { path: 'servers', component: ServersComponent }
        ]
      //...
      imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forRoot(appRoutes)
      ],
      ``` 
  
    * In the html
      Loading the Route
      ```angular2html
        <div class="row">
          <div class="col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">
            <!--  Loads currently selected Route    -->
            <router-outlet></router-outlet>
          </div>
        </div>
      ```
      Navigating the Route
      ```angular2html
        <ul class="nav nav-tabs">
          <li role="presentation" class="active"><a routerLink="/">Home</a></li>
          <li role="presentation"><a routerLink="servers">Servers</a></li>
          <li role="presentation"><a [routerLink]="['/users']">Users</a></li>
        </ul>
      ```
  * In the routerLink, if you omit `/` or use `./`, then it becomes relative path. Current URL + new path.
  If you add `/`, then it becomes the absolute path. 
      
  * Marking the current link as active: `routerLinkActive="CSS CLASS"` (`routerLinkActive="active"`). In angular, it will try to mark
  it as active if it matches any part of the URL. This includes the /. For example, if we go to `/servers`,
  then it will match with `/` and `servers`. To make it an exact match, use `[routerLinkActiveOptions]="{exact: true}"`.
    
  * Navigating in TS
  ```typescript
  constructor(private router: Router) { }
  
  onLoadServers() {
    // Complex calculation
    this.router.navigate(['/servers'])
  }
  ```

### Separating Routes from app.module.ts
1. Create `app-routing.module.ts` at the root level of the project:
```typescript
import xyz
const appRoutes: Routes = [
  { path: '',              redirectTo: '/recipes', pathMatch: 'full'},
  { path: 'recipes',      component: RecipesComponent, children: [
      { path: '',         component: RecipeStartComponent },
      { path: ':id/edit', component: RecipeEditComponent }
    ]},
]

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule] // This tells what should be accessible from other modules
})
export class AppRoutingModule {}
```

2. Import it into `app.module.ts` under `imports` section.

### Dynamic Routes
  * Use `{ path: 'users/:id', component: UserComponent },`, where it loads the UserComponent with any value for :id.
  * To load the data, inject the ActivatedRoute service:
  ```typescript
    constructor(private route: ActivatedRoute) { }
  
    ngOnInit() {
      this.user = {
      id: this.route.snapshot.params['id'],
        name: this.route.snapshot.params['name']
      }
    }
  ```
  
  * `params` as Observable
    * You can subscribe to changes in the route
    * Useful if you want to reload the same component that you are on which has already been initialized.
     ```typescript
        ngOnInit() {
        this.user = {
          id: this.route.snapshot.params['id'],
          name: this.route.snapshot.params['name']
        }
        this.route.params
          .subscribe(
            (updatedParams: Params) => {
            this.user.id = updatedParams['id']
            this.user.name = updatedParams['name']
            }
          )
        }
    ```
    * The subscription is not tied to the component but lies in memory. This is because if you navigate away to a different component, 
      Angular would have to re-create it. For the Routes, Angular automaticaly performs the cleanup but for custom subscriptions,
      you will want to destroy them. You can manually destroy the subscription with `ngDestroy`.:
      ```typescript
        paramsSubscription: Subscription
      
        this.paramsSubscription = this.route.params
          .subscribe(
            (updatedParams: Params) => {
              this.user.id = updatedParams['id']
              this.user.name = updatedParams['name']
              }
          )
      
        ngOnDestroy(): void {
          this.paramsSubscription.unsubscribe();
        }
      ```
  * Adding Extra Options to Links
    * Use `[queryParams]="{}"` to add this as a property to the routerLink directive. This itself is not a directive!
    * Use `fragment="""` to add `#STRING` to the link
    * Typescript:
      ```typescript
        this.router.navigate(['/servers', id, 'edit'], {queryParams: {allowEdit: 1}, fragment: 'loading'})
      ```
    
    * Retrieving:
      ```typescript
          console.log("queryParams: ", this.route.snapshot.queryParams)
          console.log("fragment: ", this.route.snapshot.fragment)
      ```
      
  * Nested Routes
    * Add in the `children` property: 
    ```typescript
    { path: 'servers', component: ServersComponent, children: [
       { path: ':id/edit', component: ServersComponent },
       { path: ':id', component: ServerComponent} ]
    }
    ```
    * Then add in `<router-outlet>` tag for the child component routes

  * Preserving Params across routes (`queryParamsHandling`):
  ```typescript
    this.router.navigate(['edit'], {relativeTo: this.route,
      queryParamsHandling: 'preserve'})
  ```
    
### Wildcard Routes
  * Redirect use `redirectTo: URL`
  * Wildcard use `**`. Make sure the wildcard is the last entry in the Routes definition because it gets parsed from top to bottom.
      
### Guards
  * Fine grain control for the route and child routes.
  * Implement the service:
  ```typescript
  @Injectable()
  export class AuthGuardService implements CanActivate, CanActivateChild {
    constructor(private authService: AuthService, // This is a mock service
                private router: Router) {
    }
  
    canActivate(route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.authService.isAuthenticated().then(
        (authenticated: boolean) => {
          if(authenticated) {
            return true;
          } else {
            this.router.navigate(['/']);
          }
        }
      )
    }
  
    canActivateChild(route: ActivatedRouteSnapshot,
                     state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.canActivate(route, state);
    }
  
  }
  ```
  * Add it to the Route definition:
  ````typescript
  { path: 'servers',
      // canActivate: [AuthGuardService],
      canActivateChild: [AuthGuardService],
      component: ServersComponent, children: [
        { path: ':id/edit', component: EditServerComponent },
        { path: ':id', component: ServerComponent} ]
  },
  ````
  
  * CanDeactivate: Angular will try to run this guard whenever we try to leave the route. This is useful for making sure
    the user confirms that he/she want to discard the changes.
    * In the route definition, add `{ path: ':id/edit', component: EditServerComponent, canDeactivate: [CanDeactivateGuard] },`
    * In app.module.ts, add the component to the providers list
    * Create a new service:
      ```typescript
      import {Observable} from "rxjs";
      import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from "@angular/router";
      
      export interface CanComponentDeactivate {
        canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
      }
      
      export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
      
        canDeactivate(component: CanComponentDeactivate, currentRoute: ActivatedRouteSnapshot,
                      currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean> |
          Promise<boolean> | boolean {
            return component.canDeactivate();
        }
      
      }
      ```
    * Implement this service's interface in the component that you want to have this guard:
      ```typescript
      export class EditServerComponent implements OnInit, CanDeactivateGuard {
        ...
        canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
          if(!this.allowEdit) {
            return true;
          }
          if((this.serverName !== this.server.name || this.serverStatus !== this.serverStatus) &&
              !this.changesSaved) {
            return confirm("Do you want to discard the changes?");
          } else {
            return true;
          }
        }
      
      }
      ```
  * Passing Static Data to a Route:
    * Add data property to use as key-value pairs```{ path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found!'} },```
    * Retrieve via `route.snapshot.data['key']`
    ```typescript
    ngOnInit(): void {
      // this.errorMessage = this.route.snapshot.data['message']
      this.route.data.subscribe(
        (data: Data) => {
          this.errorMessage = data['message'];
        }
      )
    }
    ```
  * Passing Dynamic Data with Resolve Guard
    * Could be use for initializing data like populating an object
    * Create a Service implementing the Resolve interface. This service will make calls to grab the server info:
      ```typescript
      interface Server {
        id: number;
        name: string;
        status: string;
      }
      
      @Injectable()
      export class ServerResolverService implements Resolve<{id: number, name: string, status:string}> {
      
        constructor(private serversService: ServersService) {}
      
        resolve(route: ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<Server> | Promise<Server> | Server {
          return this.serversService.getServer(+route.params['id']); // No need to use observable since this is run everytime
        }
      }
      ```  
    * Add it to the route: `{ path: ':id', component: ServerComponent, resolve: {server: ServerResolverService}}`
      * This adds it to the data field of the route using server as the key and the ServerResolverService.resolve() as the value
    * Finally, you can use that data in the server component with:
      ```typescript
        ngOnInit() {
          this.route.data.subscribe(
            (data: Data) => {
              this.server = data['server']
            }
          )
      }
      ```
## Observable
* Observable: various data sources: User input(events), http requests, triggered in code, etc.
  * Some may never complete. E.g., mouse click
* Observer: your code/ `subscribe` function. Handle data, error or completion = hooks. Passive meaning you have to wait for
  the event to happen.
* Async tasks so you don't have to wait! Alternative to promises and callbacks
* Creating new observable:
    ```typescript
    import {Component, OnDestroy, OnInit} from '@angular/core';
    import {interval, Observable, Subscription} from "rxjs";
    
    @Component({
      selector: 'app-section-observable',
      templateUrl: './section-observable.component.html',
      styleUrls: ['./section-observable.component.css']
    })
    
    export class SectionObservableComponent implements OnInit, OnDestroy {
      private firstObsSubscription: Subscription
      count: number
    
      constructor() {
      }
    
      ngOnInit(): void {
        // this.firstObsSubscription= interval(1000).subscribe(count => {
        //   this.count = count + 1;
        // })
    
        const customIntervalObservable = Observable.create(observer => {
          let count = 0; // This is pass in to observer.next(count)
          setInterval(() => {
            observer.next(count);
            if (count == 2) {
              observer.complete();
            }
            if (count > 3) {
              observer.error(new Error('Count is greater than 3!'));
            }
            count++;
          }, 1000)
        })
    
        this.firstObsSubscription = customIntervalObservable.subscribe(data => {
          this.count = data;
          console.log(data);
        }, error => {
          console.log(error);
          alert(error.message);
        }, () => {
          console.log('Completed');
        });
    
      }
    
      ngOnDestroy(): void {
        this.firstObsSubscription.unsubscribe();
      }
    
    }
    ```
* If you create your own subscription, then you must unsubscribe them using onDestroy. Observable that comes from Angular
automatically have that implemented.
* Error condition cancels out the observable and the next event cannot be omitted.
* Operators: Allows data to be transformed by operators before it is fetched from the subscriber.
    * Uses the pipe operators from `rxjs/operators`: 
    ```typescript
        this.firstObsSubscription = customIntervalObservable.pipe(map((data: number) => {
          return (data * 100);
        })).subscribe(data => {
          this.count = data;
          console.log(data);
        }, error => {
          console.log(error);
          alert(error.message);
        }, () => {
          console.log('Completed');
        });
    ```

* Subject: An alternative to event emitter for cross component communication using a Service IF `@Output` is not needed.
  Otherwise, still use Event Emitter. This is active meaning you can call next() from outside of the subject:
    ```typescript
    import {Injectable} from "@angular/core";
    import {Subject} from 'rxjs';
    
    @Injectable({providedIn: 'root'})
    export class SampleSubjectService {
      activatedEmitter = new Subject<boolean>();
    }
    ```
  
    ```typescript
    // Somewhere else
    this.sampleSubjectService.activatedEmitter.next(true);
    // Remember to also destroy it to prevent memory leaks
    this.sampleSubjectService.unsubscribe();
    ```
  
## Forms
* Two approaches:
    * Template-Driven: Angular infers the Form Object from the DOM. 
        * However, you will still need to tell Angular which Angular fields it needs to interact with (control).
        * You can do this by adding `ngModel` and `name=""` attributes to the fields.
        * To access the Angular form object, do this:
            ```angular2html
             <form (ngSubmit)="onSubmit(f)" #f="ngForm">
            ```
            * Where #f is the reference of the entire form and `#f="ngForm"` is we are accessing a specific part of the 
      selector which is the ngForm part. Then in the Typescript:
            ```typescript
              onSubmit(form: NgForm) {
                console.log("Submitted!");
                console.log(form);
              }
            ```
            * An alternative way is via `@ViewChild('f') signupForm: NgForm`
        * Validations
            * Add attributes from https://angular.io/api/forms/Validators
            * Angular dynamically updates the form based on events/actions from the user
            * Example of using local reference to output validation message:
                ```angular2html
                      <div class="form-group">
                        <label for="email">Mail</label>
                        <input type="email"
                               id="email"
                               class="form-control"
                               ngModel
                               name="email"
                               required
                               email
                               #email="ngModel">
                        <span class="help-block" *ngIf="!email.valid && email.touched">Please enter a valid email!</span>
                      </div>
                ```
        * To add placeholders, use `[ngModel]="VALUE"`
        * To use two-way binding, use `[(ngModel)]="answer">`
        * Group controls with `ngModelGroup="nameOfControl"`
        * To Update a Form value in place, try `this.signupForm.form.patchValue({ key: newValue })`, where `@ViewChild('f') signupForm: NgForm;`
        * To reset, use `this.signupForm.reset()`
    * Reactive: Form is created programmatically and synchronized with the DOM.
        * Form is created in Typescript
            * In Typescript:
                ```typescript
                  signupForm: FormGroup;
                
                  ngOnInit(): void {
                    this.signupForm = new FormGroup({
                      'username': new FormControl(null),
                      'email': new FormControl(null),
                      'gender': new FormControl('male')
                    });
                  }
                ```
            * In HTML, `[formGroup]` will bind it to a programmatically declared form group. `formControlName` will bind the control to a property
    of the FormGroup.
               ```angular2html
                <form [formGroup]="signupForm">
                <div class="form-group">
                  <label for="username">Username</label>
                  <input
                    type="text"
                    id="username"
                    formControlName="username"
                    class="form-control">
                </div>
               ```
            * This is how you can access a control in your form:
            ```html
                *ngIf="signupForm.get('controlName').valid"
        
            ```
            * Adding Validators: `new FormControl(null, Validators.required)`. Don't execute the function, only the reference to it.
            * Adding dynamic form controls
                ```typescript
                  onAddHobby() {
                  const control = new FormControl(null, Validators.required);
                  // Important! You have to cast this to make it work
                  (<FormArray>this.signupForm.get('hobbies')).push(control);
                  }
                ```
            * Using FormGroup's validation errors to display helpful messages:
                ```angular2html
                <span class="help-block"
                      *ngIf="!signupForm.get('userData.username').valid && signupForm.get('userData.username').touched">
                  <span *ngIf="signupForm.get('userData.username').errors['nameIsForbidden']">This name is forbidden</span>
                  <span *ngIf="signupForm.get('userData.username').errors['required']">This field is required!</span>
                </span>
                ```
          * Asynchronous validators
            ```typescript
              forbiddenEmails(control: FormControl): Promise<any> | Observable<any> {
                const promise = new Promise<any>((resolve, reject) => {
                  setTimeout(()=> {
                    if(control.value === 'test@test.com') {
                      resolve({'emailIsForbidden': true});
                    } else {
                      resolve(null);
                    }
                  }, 1500);
                })
                return promise;
              }
            
            // Somewhere later:
            'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails),
            ```
            
        * Other useful methods:
            ```typescript
            this.signupForm.valueChanges.subscribe((value)=>{
              console.log("Value changed: " , value);
            })
            this.signupForm.statusChanges.subscribe((status)=>{
              console.log("Status changed: " , status);
            })
            this.signupForm.setValue({
              'userData': {
                'username': 'Guoyang',
                'email': 'guoyang@test.com'
              },
              'gender': 'male',
              'hobbies': []
            })
            this.signupForm.patchValue({
              'userData': {
                'username': 'Guo',
              },
            })
           ```
          
## Pipes
* Applies transformation using pipe `|` operator
* Example:
    ```angular2html
        <strong>{{ server.name }}</strong> |
        {{ server.instanceType | titlecase }} |
        {{ server.started      | date:'fullDate' }}
    ```
* https://angular.io/api/common#pipes 
* Custom Pipes
    * New Class as `shorten.pipe.ts`:
        ```typescript
        import {Pipe, PipeTransform} from "@angular/core";
        
        @Pipe({
            name: 'shorten'
        })
        export class ShortenPipe implements PipeTransform{
            transform(value: any, limit: number): any {
                if(value.length > limit)
                return value.substr(0, limit) + '...';
            }
        }
        ```
        * Add the class to `app.module.ts` declarations block.
        * Then use it in the HTML referencing the @Pipe.name value
    * Filter Pipe
        * `$ ng g pipe filter`
        * TS:
            ```typescript
            import { Pipe, PipeTransform } from '@angular/core';
            
            @Pipe({
              name: 'filter',
              pure: false // NEGATIVE PERFORMANCE IMPACT. This will refresh on data update.
            })
            export class FilterPipe implements PipeTransform {
            
              transform(value: unknown, filterString: string, propName: string): unknown {
                if(value.length === 0 || filterString === '') {
                  return value;
                }
                const resultArray = [];
                for(const item of value) {
                  if (item[propName] === filterString) {
                    resultArray.push(item);
                  }
                }
                return resultArray;
              }
            
            }
            ```
      * HTML:
        ```angular2html
        class="list-group-item"
        *ngFor="let server of servers | filter:filteredStatus:'status'"
        [ngClass]="getStatusClasses(server)">
       ```
    *  `async` pipe returns the result of a Promise/Observable

## Http & Backend Interaction

        
