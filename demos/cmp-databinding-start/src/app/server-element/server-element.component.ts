import { Component, Input,
         DoCheck,
         OnChanges,
         OnInit,
         SimpleChanges,
         ViewEncapsulation } from '@angular/core';
@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ServerElementComponent implements OnInit, OnChanges, DoCheck {
  @Input('srvElement')
  element: {type: string, name: string, content: string}

  constructor() {
    console.log("constructor called")
  }

  ngOnInit(): void {
    console.log("ngOnInit called")
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("ngOnChanges called")
  }

  ngDoCheck(): void {
    console.log("ngDoCheck called")
  }

}
