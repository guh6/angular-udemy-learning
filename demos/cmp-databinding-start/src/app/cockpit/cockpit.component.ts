import {Component, OnInit, EventEmitter, Output} from '@angular/core';

/*
  Example on using Emitter Event so that this is automatically recognized by the app component.
  Key: @Output, EventEmitter requires import from angular/core.

 */
@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
  newServerName = '';
  newServerContent = '';
  @Output() serverCreated = new EventEmitter<{serverName: string, serverContent: string }>();
  @Output('aliasBlueprintCreated') blueprintCreated = new EventEmitter<{ serverName: string, serverContent: string }>();

  constructor() { }

  ngOnInit(): void {
  }

  onAddServer() {
    this.serverCreated.emit({
      serverName: this.newServerName,
      serverContent: this.newServerContent})
  }

  onAddBlueprint() {
    this.blueprintCreated.emit({
      serverName: this.newServerName,
      serverContent: this.newServerContent})
  }

  isNameEmpty() {
    return this.newServerName === '';
  }

  isContentEmpty() {
    return this.newServerContent === '';
  }

}
