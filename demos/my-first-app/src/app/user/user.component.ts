import {Component, OnInit} from "@angular/core";

@Component({
  selector: 'app-user',
  templateUrl: 'user.component.html'
})
export class UserComponent implements OnInit {
  username;
  constructor() {
    this.username = '';
  }

  ngOnInit(): void {

  }

  onResetUsernameInput(){
    this.username = '';
  }

  emptyUsername(){
    return this.username==='';
  }

}
