import {Injectable} from "@angular/core";
import {Subject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class SampleSubjectService {
  activatedEmitter = new Subject<boolean>();
}
