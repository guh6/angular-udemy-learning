import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, Observable, Subscription} from "rxjs";
import {map} from 'rxjs/operators'
import {SampleSubjectService} from "./sample-subject.service";

@Component({
  selector: 'app-section-observable',
  templateUrl: './section-observable.component.html',
  styleUrls: ['./section-observable.component.css']
})

export class SectionObservableComponent implements OnInit, OnDestroy {
  private firstObsSubscription: Subscription
  count: number

  constructor(private sampleSubjectService: SampleSubjectService) {
  }

  ngOnInit(): void {
    // this.firstObsSubscription= interval(1000).subscribe(count => {
    //   this.count = count + 1;
    // })

    const customIntervalObservable = Observable.create(observer => {
      let count = 0; // This is pass in to observer.next(count)
      setInterval(() => {
        observer.next(count);
        if (count == 2) {
          observer.complete();
        }
        if (count > 3) {
          observer.error(new Error('Count is greater than 3!'));
        }
        count++;
      }, 1000)
    })


    this.firstObsSubscription = customIntervalObservable.pipe(map((data: number) => {
      return (data * 100);
    })).subscribe(data => {
      this.count = data;
      console.log(data);
    }, error => {
      console.log(error);
      alert(error.message);
    }, () => {
      console.log('Completed');
    });

    this.sampleSubjectService.activatedEmitter.next(true);


  }

  ngOnDestroy(): void {
    this.firstObsSubscription.unsubscribe();
  }

}
