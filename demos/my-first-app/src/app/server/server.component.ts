import {Component, Input} from "@angular/core";
import {Server} from "../shared/server.model";

// Tells Angular that this is a Component and not just a regular class
@Component({
  selector: 'app-server', // This is a UNIQUE string that is used in the HTML to reference this component
  templateUrl: 'server.component.html', // Relative Path bundled by webpackss
  // This is tested with ngClass
  styles: [`
    .online {
      color: white;
    }
  `
  ]
})
export class ServerComponent {
  // serverId = 10;
  // @Input() serverName: string
  // serverStatus: string;
  @Input() server: Server

  constructor() {
  }

  getServerStatus() {
    return this.server.status;
  }

  getColor() {
    return this.server.status === 'online' ? 'green' : 'red'
  }

}
