import {Component, OnInit} from '@angular/core';
import {Server} from "../shared/server.model";

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverCreationStatus = 'No server was created!';
  serverName = 'Test'
  serverCreated = false;
  servers = [new Server("Default-1"), new Server("Default-2")];

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000)
  }

  ngOnInit(): void {
  }

  onCreateServer() {
    this.serverCreationStatus = 'Server was created. The server name is ' + this.serverName;
    this.serverCreated = true
    this.servers.push(new Server(this.serverName));
  }

  onUpdateServerName(event: any) {
    console.log(event)
    this.serverName = (<HTMLInputElement>event.target).value;
  }

}
