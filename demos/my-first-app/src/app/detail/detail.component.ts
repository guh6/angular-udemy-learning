import {Component} from "@angular/core";

@Component({
  selector: 'app-detail',
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.css']
})
export class DetailComponent {
  displayDetails;
  clickedTimestamp;

  constructor() {
    this.displayDetails = false;
    this.clickedTimestamp = [];
  }

  onToggleDisplay(){
    this.displayDetails = true;
    this.clickedTimestamp.push(new Date())
  }


}
