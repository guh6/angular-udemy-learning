import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {ServerComponent} from "./server/server.component";
import { ServersComponent } from './servers/servers.component';
import { WarningAlertComponent } from './warning-alert/warning-alert.component';
import {SuccessAlertComponent} from "./success-alert/success-alert.component";
import {UserComponent} from "./user/user.component";
import {DetailComponent} from "./detail/detail.component";
import { NumberComponent } from './section7/number/number.component';
import {BasicHighlightDirective} from "./section7/basic-highlight/basic-highlight.directive";
import {BetterHighlightDirective} from "./section7/better-highlight/better-hightlight.directive";
import {UnlessDirective} from "./section7/unless/unless.directive";
import { SectionObservableComponent } from './section-observable/section-observable.component';

// import { HttpModule } from '@angular/http';
@NgModule({
  // Internal Component Definitions
  declarations: [
    AppComponent,
    ServerComponent,
    ServersComponent,
    WarningAlertComponent,
    SuccessAlertComponent,
    UserComponent,
    DetailComponent,
    NumberComponent,
    BasicHighlightDirective, // This is a custom Directive
    BetterHighlightDirective,
    UnlessDirective,
    SectionObservableComponent, // This is a custom Structural Directive
  ],
  // External Module dependencies
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
