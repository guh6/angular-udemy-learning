export class Server {
  public status: string
  public id: number

  constructor(public name: string) {
    this.status = Math.random() > 0.5 ? 'online' : 'offline';
    this.id = this.randomInteger(1,8000);
  }

  private randomInteger(min: number, max: number) {
    return Math.floor(Math.random() * (max - min +1)) + min;
  }
}
