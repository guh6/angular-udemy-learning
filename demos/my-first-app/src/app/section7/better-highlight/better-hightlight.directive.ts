import {Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2} from "@angular/core";

@Directive({
  selector: '[appBetterHighlight]' // [] let you use the directive without the square brackets to an element
})
export class BetterHighlightDirective implements OnInit {
  // Sets a default if it's null in the beginning
  @Input() defaultColor: string = 'transparent';
  @Input() highlightColor: string = 'blue';
  // access the corresponding property for the element
  @HostBinding('style.backgroundColor') backgroundColor: string = this.defaultColor;

  constructor(private elRef: ElementRef, private renderer: Renderer2) {

  }
  ngOnInit(): void {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
  }

  @HostListener('mouseenter') mouseover(eventData: Event) {
    // One way to do this is through renderer
    // this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue')
    // Another way is to do it through
    this.backgroundColor = this.highlightColor;
  }

  @HostListener('mouseleave') mouseleave(eventData: Event) {
    // this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent')
    this.backgroundColor = this.defaultColor;
  }
}
