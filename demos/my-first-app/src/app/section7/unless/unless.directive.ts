import {Directive, Input, TemplateRef, ViewContainerRef} from "@angular/core";

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {
  // Creates a custom function for this input
  // The method name HAS to be the same as the selector name
  @Input() set appUnless(condition: boolean) {
    if(!condition) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainerRef.clear();
    }
  }

  // templateRef - what
  // viewContainerRef- where
  constructor(private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {

  }
}
