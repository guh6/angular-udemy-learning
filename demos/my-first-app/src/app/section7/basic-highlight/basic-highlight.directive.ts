import {Directive, ElementRef, OnInit} from "@angular/core";

@Directive({
  selector: '[appBasicHighlight]' // [] let you use the directive without the square brackets to an element
})
export class BasicHighlightDirective implements OnInit {
  constructor(private elementRef: ElementRef) {

  }

  ngOnInit(): void {
    this.elementRef.nativeElement.style.backgroundColor = 'green'; // Remember, this is bad practice to assign value using elementRef
  }
}
