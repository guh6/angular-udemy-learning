import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.css']
})
export class NumberComponent implements OnInit {
  numbers = [1, 2, 3, 4, 5];
  oddNumbers = [1,3,5]
  evenNumbers = [2,4]
  onlyOdd = false;
  constructor() { }

  ngOnInit(): void {
  }

}
