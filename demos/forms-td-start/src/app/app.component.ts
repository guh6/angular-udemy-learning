import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('f') signupForm: NgForm;
  public defaultQuestion: String = "pet";
  public answer: String = '';
  public genders: String[] = ['male', 'female'];
  private suggestedName = ['Superuser', 'Bathman', 'Superman', 'Wonderwoman'];
  user = {
    username: '',
    email: '',
    secretQuestion: '',
    answer: '',
    gender: ''
  }
  submitted: Boolean = false;

  suggestUserName() {
    this.signupForm.form.patchValue({
      username: this.suggestedName[Math.floor(Math.random() * this.suggestedName.length)]
    })
  }

  onSubmit(form: NgForm) {
    console.log("Submitted!");
    console.log(form);

    console.log("Sign Up Form");
    console.log(this.signupForm)

    this.user.username = this.signupForm.value.username;
    this.user.email = this.signupForm.value.email;
    this.user.secretQuestion = this.signupForm.value.secret;
    this.user.answer = this.signupForm.value.questionAnswer
    this.user.gender = this.signupForm.value.gender

    this.submitted = true;
    this.signupForm.resetForm();
  }

}
