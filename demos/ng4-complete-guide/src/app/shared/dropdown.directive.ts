import {Directive, ElementRef, HostBinding, HostListener} from "@angular/core";

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  // This binds to the element's class definition and adds open to the class if true.
  @HostBinding('class.open') isOpen: boolean = false;

  // @HostListener('click') toggleOpen() {
  //   this.isOpen = !this.isOpen;
  // }
  //
  @HostListener('document:click', ['$event']) toggleOpen(event: Event) {
    // console.log(this.elRef.nativeElement);
    // console.log(event.target);
    this.isOpen = this.elRef.nativeElement.contains(event.target) ? !this.isOpen : false;
  }

  constructor(private elRef: ElementRef) {
  }
}
