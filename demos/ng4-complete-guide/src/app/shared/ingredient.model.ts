export class Ingredient {
  // using public automatically creates the accessor
  constructor(public name: string, public amount: number) {
    this.name = name;
    this.amount = amount;
  }
}
