import {EventEmitter, Injectable} from "@angular/core"
import {Recipe} from "./recipe.model";
import {Ingredient} from "../shared/ingredient.model";
import {ShoppingListService} from "../shopping-list/shopping-list.service";
import {Subject} from "rxjs";

/**
 * Manages our recipes
 */
@Injectable()
export class RecipeService {
  private _recipes: Recipe[] = [
    new Recipe(
      0,
      'Tasty Schnitzel',
      'A super-tasty Schnitzel!',
      'https://cdn.pixabay.com/photo/2014/12/21/23/28/recipe-575434_960_720.png',
      [new Ingredient('Meat', 1), new Ingredient('Fries', 2)]),
    new Recipe(
      1,
      'Big Fat Burger',
      'What else you need to say?',
      'https://cdn.pixabay.com/photo/2014/12/21/23/28/recipe-575434_960_720.png',
      [new Ingredient('Ground Meat', 1), new Ingredient('Buns', 2)])
  ];

  // get recipes(): Recipe[] {
  //   return this._recipes.slice(); // Returns a copy
  // }

  constructor(private shoppingListService: ShoppingListService) {
  }

  getRecipes(): Recipe[] {
    return this._recipes.slice();
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }

  getRecipe(id: number) {
    return this._recipes[id];
  }
}
