import {Ingredient} from "../shared/ingredient.model";
import {Subject} from 'rxjs';

export class ShoppingListService {
  public onNewIngredient = new Subject<Ingredient[]>();

  private _ingredients: Ingredient[] = [
    new Ingredient("Roma Tomato", 2),
    new Ingredient("Yam", 5)
  ];

  public getIngredients() {
    return this._ingredients.slice();
  }

  public onNewIngredientAdded(ingredient: Ingredient) {
    this._ingredients.push(ingredient);
    this.onNewIngredient.next(this._ingredients.slice());
  }

  /**
   * ALternative approach to add ingredients to the shopping list
   * @param ingredients
   */
  public onNewIngredientListAdded(ingredients: Ingredient[]) {
    this._ingredients = this._ingredients.concat(ingredients.slice());
  }

  public addIngredients(ingredients: Ingredient[]) {
    this._ingredients.push(...ingredients); // Spread operator
    this.onNewIngredient.next(this._ingredients.slice());
  }

}
